/**
 * Created by priitpl.
 */

'use strict';


let chai = require('chai');
let chaiHttp = require('chai-http');
let assert = chai.assert;
let should = chai.should();
let BaseApi = require('./../../lib/models/base');


describe('Test Group model', () => {
    let User = BaseApi.getModel('users');

    // Data to be tested on user
    let data = {
        username: 'user@name.ee',
        password: 'password'
    }



    it('Should validate data', done => {
        let user = new User();

        // should be not valid agains empty data
        let isValid = user.isValid({});
        isValid.should.be.false;

        // email is not valid.
        isValid = user.isValid({username: 'not_valid', password: 'valid'});
        isValid.should.be.false;

        // should be valid against filled correct data
        isValid = user.isValid(data);
        isValid.should.be.true;

        done();
    });

    it('Should fill user with data', done => {
        let user = new User(data);

        user.get('username').should.be.equal(data.username);
        user.get('password').should.be.equal(data.password);
        done();
    });



    let clearUpId;
    it("Should successfully store in database", done => {
        let user = new User();
        user.fill(data);
        var a = user.save()
                .then(id => {
                    id.should.be.int;
                    clearUpId = id;
                    done();
                })
                .catch(e => {
                    done(e);
                })
            ;
    });

    it("Should remove successfully", done => {
        User.delete(clearUpId).then(result => {
            result.should.be.boolean;
            done();
        })
            .catch(e => {
                done(e);
            });
    });


});