/**
 * Created by priitpl.
 */

'use strict';

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let BaseApi = require('./../lib/models/base');

chai.use(chaiHttp);

describe('Test Models', () => {
    it('Base should get list of models', done => {
        BaseApi.getModels().should.be.a('array');
        done();
    });

    it('First loaded module should be an object', done => {
        let modelName = BaseApi.getModels()[0];
        let model = BaseApi.getModel(modelName);
        model.should.be.a('function');
        done();
    });

    describe('Test models for getFields() method', () => {
        let models = BaseApi.getModels();
        for(let modelName of models) {
            let model = BaseApi.getModel(modelName);
            it(`Model: '${modelName}' should have list of fields`, done => {
                model.getFields().should.be.a('object');
                done();
            });
        }
    });

    describe('Test models should have own tables', () => {
        let models = BaseApi.getModels();
        for(let modelName of models) {
            let model = BaseApi.getModel(modelName);
            it(`Model: '${modelName}' should have list of fields`, done => {
                model.getTableName().should.not.be.empty;
                done();
            });
        }
    });
});