/**
 * Created by priitpl.
 */

'use strict';


const chai = require('chai');
const should = chai.should();
const BaseModel = require('../../lib/models/base');
const invoiceGenerator = require('../../lib/invoice_generator');

const Group = BaseModel.getModel('groups');
const Price = BaseModel.getModel('prices');
const Invoice = BaseModel.getModel('invoices');
const ParkingRecord = BaseModel.getModel('records');

const CAR_NUMBER = '123ABC';


describe('Test VIP client invoice calculation', () => {

    describe('example 2 total sum should be equal 38.25 EUR', () => {
        // Create Premium group
        let group = new Group({
            name: 'Premium customer',
            is_default: false,
            monthly_fee: 20,
            maximum: 300
        });

        /**
         * Group prices
         */
        let prices = [];

        /**
         * Every 30 minutes fee
         */
        prices.push(new Price({
            apply_once: false,
            cost: 1.00,
            every_minutes: 30,
            time_since: '07:00',
            time_till: '19:00'
        }));

        /**
         * Every 30 minutes fee
         */
        prices.push(new Price({
            apply_once: false,
            cost: 0.75,
            every_minutes: 30,
            time_since: '19:00',
            time_till: '07:00'
        }));


        /**
         * Parking records
         */
        let parkingRecords = [
            [new ParkingRecord({
                car_number: CAR_NUMBER,
                started_at: new Date('2016-08-20 08:12'),
                finished_at: new Date('2016-08-20 10:45'),
            }), 6.00],
            [new ParkingRecord({
                car_number: CAR_NUMBER,
                started_at: new Date('2016-08-21 07:02'),
                finished_at: new Date('2016-08-21 11:56'),
            }), 10.00],
            [new ParkingRecord({
                car_number: CAR_NUMBER,
                started_at: new Date('2016-08-22 22:10'),
                finished_at: new Date('2016-08-22 22:35'),
            }), 0.75],
            [new ParkingRecord({
                car_number: CAR_NUMBER,
                started_at: new Date('2016-08-23 19:40'),
                finished_at: new Date('2016-08-23 20:35'),
            }), 1.50]
        ];


        parkingRecords.forEach(record => {
            it(`parking cost must be equals to ${record[1]}`, done => {
                let cost = invoiceGenerator.calculateParkingPrice(record[0], prices);
                cost.should.be.equals(record[1]);
                done();
            });
        });

        it ('total cost should be equals to 38.25', done => {
            let totalSum = invoiceGenerator.calculateGroupBasedSum(group, prices, parkingRecords.map(record => record[0]));
            totalSum.should.be.equals(38.25);
            done();
        });
    });
});