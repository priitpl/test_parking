/**
 * Created by priitpl.
 */

'use strict';


const chai = require('chai');
const should = chai.should();
const expect = chai.expect();
const BaseModel = require('../../lib/models/base');
const rewire = require('rewire');
const generator = rewire('../..//lib/invoice_generator');


let getMinutesUsedInRange = generator.__get__('getMinutesUsedInRange');

describe('Test invoice generation helpers', () => {

    describe('Test minutes used in range', () => {

        let times = [
            {
                since: '00:00',
                till: '00:00',
                started: new Date('2016-08-20 10:00'),
                finished: new Date('2016-08-20 11:00'),
                result: 0
            },
            {
                since: '10:30',
                till: '12:30',
                started: new Date('2016-08-20 10:00'),
                finished: new Date('2016-08-20 11:00'),
                result: 30
            },
            {
                since: '07:00',
                till: '19:00',
                started: new Date('2016-08-20 10:00'),
                finished: new Date('2016-08-20 11:00'),
                result: 60
            },
            {
                since: '07:00',
                till: '19:00',
                started: new Date('2016-08-20 08:12'),
                finished: new Date('2016-08-20 10:45'),
                result: 153
            },
            {
                since: '07:00',
                till: '19:00',
                started: new Date('2016-08-20 07:02'),
                finished: new Date('2016-08-20 11:56'),
                result: 294
            },
            {
                since: '07:00',
                till: '19:00',
                started: new Date('2016-08-20 22:10'),
                finished: new Date('2016-08-20 22:35'),
                result: 0
            },
            {
                since: '07:00',
                till: '19:00',
                started: new Date('2016-08-20 19:40'),
                finished: new Date('2016-08-20 20:35'),
                result: 0
            },
            {
                since: '19:00',
                till: '07:00',
                started: new Date('2016-08-20 19:40'),
                finished: new Date('2016-08-20 20:35'),
                result: 55
            },
            {
                since: '19:00',
                till: '07:00',
                started: new Date('2016-08-20 19:40'),
                finished: new Date('2016-08-21 20:35'),
                result: 680
            },
            {
                since: '13:00',
                till: '15:00',
                started: new Date('2016-08-20 19:40'),
                finished: new Date('2016-08-21 20:35'),
                result: 0
            }
        ];

        times.forEach(time => {
            it(`should return ${time.result} minutes for timerange: ${time.since} - ${time.till}`, done => {
                let result = getMinutesUsedInRange(time.since, time.till, time.started, time.finished);
                result.should.be.equals(time.result);
                done();
            });
        });
    });
});