/**
 * Created by priitpl.
 */

'use strict';

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let assert = chai.assert;
let httpResponse = require('./../lib/http_response');


let response = {
    json: function(data){
        return data
    }
}

describe('Test HTTP Responses', () => {

    let errorMessage = "test error message";

    it('should generate simple correct onError', done => {
        new httpResponse.onError(new Error(errorMessage)).json({json: data => {
            assert.equal(false, data.success);
            assert.equal(errorMessage, data.error, "Error is differ");
            done();
        }});
    });

    it('should generate simple proper onSuccess', done => {
        let testInfo = {
            item: {
                id: 1,
                name: "test"
            }
        };

        new httpResponse.onSuccess(testInfo).json({json: data => {
            assert.equal(data.success, true);
            assert.equal(data.item, testInfo.item);
            done();
        }});
    });
});