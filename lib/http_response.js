/**
 * Created by priitpl.
 */

'use strict';
const merge = require('merge');


class onError extends Error{
    constructor(error){
        let message = error;
        if (typeof  error == 'object') {
            if ('message' in error) {
                message = error.message;
            } else {
                message = error.toString();
            }
        }
        super(message);
        this.data = null;
        this.fieldErrors = {};
    }

    addFieldError(field, error){
        this.fieldErrors[field].push(error);
        return this;
    }

    json(res){
        let data = {
            success: false,
            error: this.message
        };

        if (this.fieldErrors.size > 0) {
            data = merge(data, this.fieldErrors);
        }
        res.json(data);
    }
}

class onSuccess{
    constructor(data){
        this.data = data || {};
    }

    json(res){
        let data = {
            success: true
        };

        if (Object.keys(this.data).length > 0){
            data = merge(data, this.data);
        }

        res.json(data);
    }
}

module.exports = {
    onError: onError,
    onSuccess: onSuccess
};