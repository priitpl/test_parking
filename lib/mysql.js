/**
 * Created by priitpl.
 */


'use strict';

const nconf     = require('./config');
const Promise   = require('bluebird');
const mysql     = require('mysql');

/**
 * Export only query method.
 * Pooling will be used for all queries.
 */
module.exports = query;

/**
 * Get Database config.
 */
const DB_CONF = nconf.get('db');

/**
 * Create Pooling
 */
let MysqlPool = mysql.createPool({
    host     : DB_CONF.host,
    port     : DB_CONF.port,
    user     : DB_CONF.username,
    password : DB_CONF.password,
    database : DB_CONF.database,
    connectTimeout: 5000,
    acquireTimeout: 5000
});


/**
 * Make query
 * @param query
 * @param args
 */
function query(query, args){
    // console.log("Make query", query, args);
    return new Promise((resolve, reject) => {
        MysqlPool.getConnection((err, connection) => {
            if (err) {
                throw err;
            }

            // Make query
            connection.query(query, args, (err, rows) => {
                // release connection first
                connection.release();

                if (err) {
                    resolve([err, null]);
                    // Can't rethrow error, will crash.
                    // throw err;

                } else {
                    resolve([null, rows]);
                }

            });
        });
    })
    /**
     * This is hack due to the problem of
     * rethrowing error in query. it will crash with rethrowing problem.
     * So, in previous, if error appears, we resolve with old logic (err, data)
     * and catch here manually. if first element is not null, we throw an error,
     * otherwise return second element.
     */
        .then((data) => {

        if (data[0]) {
            throw data[0];

        } else {
            return data[1];

        }
    })
        ;
}