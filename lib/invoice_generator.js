/**
 * Created by priitpl.
 */


'use strict';



module.exports = {
    calculateGroupBasedSum: calculateGroupBasedSum,
    calculateParkingPrice: calculateParkingPrice
};


/**
 * Generate total sum according to group rules,
 * prices and parking records.
 *
 * @param group
 * @param prices
 * @param parkingRecords
 * @returns {number}
 */
function calculateGroupBasedSum(group, prices, parkingRecords) {
    let totalSum = 0;
    // Put monthly fee
    totalSum += group.get('monthly_fee');

    // Process each parking record, calculate price for it and store in parkingPrices
    let parkingPrices = parkingRecords.map(record => calculateParkingPrice(record, prices));

    // count total sum for parking.
    totalSum += parkingPrices.reduce((a, b) => a + b, 0);

    // return total sum as is, if max_sum not defined.
    if (group.get('maximum') === null){
        return totalSum;
    }

    return Math.min(totalSum, group.get('maximum'));
}


/**
 *
 * @param prices
 * @param parkingRecord
 */
function calculateParkingPrice(parkingRecord, prices) {
    let totalSum = 0;

    let startedAt = parkingRecord.get('started_at');
    let finishedAt = parkingRecord.get('finished_at');

    if (!finishedAt) {
        finishedAt = new Date();
    }

    const daysParked = Math.ceil(daysDiff(startedAt, finishedAt));

    prices.forEach((price, i) => {
        let minutesUsed = 0;

        for(let i = 0; i < daysParked; i++) {
            let s = new Date(startedAt.getTime());
            s.setDate(s.getDate() + i);
            minutesUsed += getMinutesUsedInRange(price.get('time_since'), price.get('time_till'), s, finishedAt);
        }

        if (price.get('apply_once')) {
            totalSum += price.get('cost');
        } else {
            totalSum += Math.ceil(minutesUsed / price.get('every_minutes')) * price.get('cost');
        }
    });

    return totalSum;
}


/**
 * @param timeSince
 * @param timeTill
 * @param started
 * @param finished
 * @returns {number}
 */
function getMinutesUsedInRange(timeSince, timeTill, started, finished){

    if (timeSince == timeTill) {
        return 0;
    }

    // set finished to current date, if it's not set.
    finished = finished || new Date();

    // get since and till time parts.
    let timeSinceParts = timeSince.match(/([\d]{2}):([\d]{2})/);
    let timeTillParts  = timeTill.match(/([\d]{2}):([\d]{2})/);


    /**
     * Get range when the price is active.
     */
    // Convert since to since time rule according to started date
    timeSince = new Date(started.getTime());
    timeSince.setHours(timeSinceParts[1]);
    timeSince.setMinutes(timeSinceParts[2]);

    timeTill = new Date(started.getTime());
    timeTill.setHours(timeTillParts[1]);
    timeTill.setMinutes(timeTillParts[2]);

    // add 1 day, is since is greater then till
    if (timeSince.getTime() > timeTill.getTime()) {
        timeTill.setDate(timeTill.getDate() + 1);
    }

    // Do not count, if starting time is ahead of till rule.
    if (started.getTime() > timeTill.getTime()){
        return 0;
    }

    // Do not count, if since is ahead of finished.
    if (timeSince.getTime() > finished.getTime()){
        return 0;
    }

    let from = Math.max(timeSince.getTime(), started.getTime());
    let till = Math.min(timeTill.getTime(), finished.getTime());

    return Math.ceil((till - from) / 1000 / 60);
}



function daysDiff(start, end){
    return hoursDiff(start, end) / 24;
}

function hoursDiff(start, end){
    return minutesDiff(start, end) / 60;
}

function minutesDiff(start, end){
    return (end.getTime() - start.getTime()) / 1000 / 60;
}