/**
 * Created by priitpl.
 */
'use strict';

let BaseApi = require('./base');


class ParkingPlace extends BaseApi {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_places';
    }

    static getFields(){
        return {
            name: {nullable: false},
            code: {
                nullable: true
            },
            address: {
                nullable: true
            },
            parking_slots: {
                nullable: true
            },
            parking_company_id:{
                nullable: true
            },
            valid_since: {
                type: 'date',
                nullable: true
            },
            valid_till: {
                type: 'date',
                nullable: true
            }
        }
    }
}

module.exports = ParkingPlace;