/**
 * Created by priitpl.
 */
'use strict';

let BaseModel = require('./base');


class PlacePrices extends BaseModel {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_places_prices';
    }

    static getFields(){
        return {
            place_id: {
                nullable: false,
                external: BaseModel.getModel('places')
            },
            group_id: {
                nullable: true,
                external: BaseModel.getModel('groups')
            },
            price_rule_id: {
                nullable: false,
                external: BaseModel.getModel('price_rules')
            },
            valid_since: {
                type: 'date',
                nullable: true
            },
            valid_till: {
                type: 'date',
                nullable: true
            }
        }
    }
}

module.exports = PlacePrices;