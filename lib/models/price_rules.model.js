/**
 * Created by priitpl.
 */
'use strict';

let BaseModel = require('./base');


class PriceRule extends BaseModel {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_price_rules';
    }

    static getFields(){
        return {
            name: {
                nullable: false
            }
        }
    }


    /**
     * Get list of prices for given rule.
     */
    getPrices(){

    }
}

module.exports = PriceRule;