/**
 * Created by priitpl.
 */
'use strict';

let BaseModel = require('./base');


class ParkingCompany extends BaseModel {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_companies';
    }

    static getFields(){
        return {
            name: {
                nullable: false
            }
        }
    }
}

module.exports = ParkingCompany;