/**
 * Created by priitpl.
 */

'use strict';

const fs = require('fs');
const Promise = require('bluebird');
const nconf = require('../config');
const mysqlQuery = require('../mysql');
let merge = require('merge');
const response = require('../http_response');

const SUFFIX = 'model';
const MODEL_REG = new RegExp(`\.${SUFFIX}\.js$`);

/**
 * Retrieve all models names
 */
const modelsNames = fs.readdirSync(__dirname)
    .filter(file => file.match(MODEL_REG))
    .map(file => file.replace(MODEL_REG, ''))
;

/**
 * Preload models here
 */
let models = {};


/**
 * Base model
 */
module.exports = class BaseModelEntry
{
    /**
     * Model constructor
     */
    constructor(data)
    {
        data = data || {};

        // Current data.
        this.data = {};

        // Data came from database.
        this.db_data =  {};

        // Fill item data with empty values
        for(let field in this.constructor.getFields()) {
            this.data[field] = null;
            this.db_data[field] = null;
        }

        // Now, fill element with data from constructor.
        this.fill(data);
    }

    /**
     * Fill item with given data
     * @param data
     */
    fill(data){
        let fields = this.constructor.getFields();
        for(let field in data){
            if (!(field in fields)){
                continue;
            }
            this.data[field] = data[field];
        }

        if (this.constructor.getIdField() in data) {
            this.id = data[this.constructor.getIdField()];
        }

        return this;
    }

    /**
     * validate
     * @todo create validation to find bad/not filled fields.
     */
    isValid(data){
        return false;
    }


    /**
     * Save record
     */
    save(){
        if (this.getId()){
            return mysqlQuery(
                `UPDATE ?? SET ? WHERE ?`,
                [this.constructor.getTableName(), this.data, this.constructor.getIdPrepared(this.getId())]
            );
        } else {
            return mysqlQuery(`INSERT INTO ?? SET ?`, [this.constructor.getTableName(), this.data])
                .then(result => Promise.resolve(result.insertId))
            ;
        }
    }

    /**
     * Delete record
     */
    delete(){
        return new Promise((resolve, reject) => {
            if (!this.getId()) {
                throw new Error("Record not stored");
            }
            return mysqlQuery(`DELETE FROM ${this.constructor.getTableName()} WHERE ?`, [this.constructor.getIdPrepared(this.getId())]);
        });
    }

    getId(){
        return this.id;
    }

    get(fieldName){
        return this.data[fieldName];
    }

    getData(){
        return merge(this.data, this.constructor.getIdPrepared(this.getId()));
    }


    /**
     *
     * Static methods
     *
     */

    static getIdPrepared(id){
        let data = {};
        data[this.getIdField()] = id;
        return data;
    }

    /**
     * bind all models to express server app
     * with given API prefix key.
     * @param apiPrefix
     * @param app
     * @param express
     */
    static bind(apiPrefix, app, express){
        for(let model in models) {
            console.log(`route: ${apiPrefix}/${model}`);
            app.use(`${apiPrefix}/${model}`, models[model].setRoutes(express.Router()));
        }
    }

    /**
     * Set express server routes for given model.
     * @param router
     * @returns {*}
     */
    static setRoutes(router){
        /*
         * POST Action.
         */
        router.route("/")
            .get((req, res) => {
                console.log("GET REQUEST");
                this.findAll()
                    .then(result => {
                        new response.onSuccess({items: result})
                            .json(res);

                    })
                    .catch(e => new response.onError(e).json(res));
            })
            .post((req, res) => {
                console.log("POST", req.body);
                let item;
                // Save
                if ('id' in req.body && req.body.id) {
                    item = this.find(req.body.id);
                    // If not found, return error
                    if (!item) {
                        return new response.onError("Record not found").json(res);
                    }
                }
                // Create
                else {
                    new this(req.body)
                        .save()
                        .then(item => new response.onSuccess({item: item.getData()}).json(res))
                        .catch(e => new response.onError(e).json(res))
                    ;

                }
            })
            ;

        /**
         * Find record by id
         */
        router.get('/:id', (req, res) => {
            this.checkId(req, res)
                .then(id => this.find(id))
                .then(
                    record => {
                        res.json({
                            success: true,
                            item: record.getData(),
                        });
                    },
                    message => {
                        res.json({
                            success: false,
                            error: "Record not found"
                        });
                    }
                )
                .catch(e => {
                    res.json({
                        success:false,
                        error: e.toString()
                    });
                })
            ;
        });


        /*
         * Delete Action
         */
        router.delete('/:id', (req, res) => {
            let item = this.find(req.param('id'));
            if (!item) {
                return res.json({
                    success: false,
                    error: "Record not found"
                });
            }

            item.delete();
            res.json({
                success: true
            });

        });

        return router;
    }

    static checkId(req){
        return new Promise((resolve, reject) => {
            if (!('id' in req.params)) {
                throw new Error("Id is missing");
            }
            if (!req.params.id) {
                throw new Error("Id is missing");
            }
            resolve(req.params.id);
        });
    }


    /**
     * Delete record by id
     * @param id
     * @returns {*}
     */
    static delete(id){
        return mysqlQuery("DELETE FROM ?? WHERE ?", [this.getTableName(), this.getIdPrepared(id)]);
    }

    /**
     * Find record by id
     * @param id
     * @returns {Promise.<T>|*}
     */
    static find(id){
        let whereId = {};
        whereId[this.getIdField()] = id;
        return mysqlQuery(`SELECT * FROM ${this.getTableName()} WHERE ?`, [whereId])
            .then(result => {
                if (result.length >= 1) {
                    return new this(result[0]);
                }
                return null;
            }).catch(e => {
                throw e;
            });
    }

    static findBy(params){
        return mysqlQuery("SELECT * FROM ??", [this.getTableName()]);
    }

    static getTableName(){
        throw new Error("Not implemented");
    }

    static getFields(){
        return {
            // created_at: {
            //     nullable: false,
            //     type: 'datetime'
            // },
            // updated_at: {
            //     nullable: false,
            //     type: 'datetime'
            // }
        };
    }

    static findAll(){
        return mysqlQuery("SELECT * FROM ??", [this.getTableName()]);
    }

    static getIdField(){
        return 'id';
    }
}

/**
 * Get Models list
 */
module.exports.getModels = function(){
    return modelsNames;
}

/**
 * Get model
 * @param name - model name
 */
module.exports.getModel = function(name){
    return models[name];
}

/**
 * preload all models
 */
for(let model of modelsNames){
    models[model] = require(`./${model}.${SUFFIX}.js`);
}



