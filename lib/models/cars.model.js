/**
 * Created by priitpl.
 */
'use strict';

let BaseModel = require('./base');


class Car extends BaseModel {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'cars';
    }

    static getFields(){
        return {
            car_number: {
                nullable: false
            }
        }
    }
}

module.exports = Car;