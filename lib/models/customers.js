/**
 * Created by priitpl.
 */

'use strict';

var BaseApiEntry = require('./base');

class Customer extends BaseApiEntry{
    constructor(){
        super();
    }

    static getTableName(){
        return 'customers';
    }

    static getFields(){
        return {

        }
    }
}

module.exports = Customer;