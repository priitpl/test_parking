/**
 * Created by priitpl.
 */
'use strict';

let BaseModel = require('./base');


class Price extends BaseModel {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_prices';
    }

    static getFields(){
        return {
            price_rule_id: {
                nullable: false,
                external: BaseModel.getModel('price_rules')
            },
            /**
             * Apply once
             */
            apply_once: {
                nullable: false,
                default: false,
                type: 'boolean'
            },
            /**
             * Parking cost.
             * If apply_once is defined, will add this cost.
             * otherwise will count total minutes usage.
             */
            cost: {
                nullable: false,
                default: 0,
                type: 'float'
            },

            /**
             * Minutes iterator.
             * Cost per each started 30 minutes.
             */
            every_minutes: {
                nullable: false,
                default: 30,
                type: 'integer'
            },

            /**
             * Maximum cost per parking.
             * If set to null, the cost will as is,
             * otherwise the cost will be the minimum of total cost per parking or maximum
             */
            maximum: {
                nullable: true,
                default: null,
                type: 'integer'
            },

            /**
             * Time since. When this price is applied
             */
            time_since:{
                type: 'time',
                nullable: false,
                default: '00:00'
            },

            /**
             * Time till. Till what time parking is applied.
             */
            time_till:{
                type: 'time',
                nullable: false,
                default: '00:00'
            }
        }
    }
}

module.exports = Price;