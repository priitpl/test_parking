/**
 * Created by priitpl.
 */
'use strict';

let BaseApi = require('./base');


class Contract extends BaseApi {
    constructor(data){
        super(data);

    }

    static getFields() {
        return {
            contract_number: {
                nullable: true
            },
            company_id:{
                nullable: false,
                external: BaseApi.getModel('companies')
            },
            car_id: {
                nullable: false,
                external: BaseApi.getModel('cars')
            },
            client_id:{
                nullable: true
            },
            valid_since: {
                type: 'date',
                nullable: false
            },
            valid_till: {
                type: 'date',
                nullable: true
            }

        };
    }

    static getTableName(){
        return 'contracts';
    }
}

module.exports = Contract;