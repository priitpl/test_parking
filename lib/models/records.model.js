/**
 * Created by priitpl.
 */
'use strict';

let BaseApi = require('./base');


class ParkingRecord extends BaseApi {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_records';
    }

    static getFields(){
        return {
            place_id: {
                nullable: false,
                external: BaseApi.getModel('places')
            },
            car_number: {
                nullable: false
            },
            car_id:{
                nullable: false,
                external: BaseApi.getModel('cars')
            },
            started_at: {
                type: 'datetime',
                nullable: false
            },
            finished_at:{
                type: 'datetime',
                nullable: true
            },
            total_minutes:{
                nullable: true,
                type: 'integer'
            }
        }
    }
}

module.exports = ParkingRecord;