/**
 * Created by priitpl.
 */
'use strict';

let BaseApi = require('./base');


class UserRole extends BaseApi {
    constructor(data){
        super(data);
    }

    static getFields() {
        return {
            name: {
                nullable: false
            }

        };
    }

    static getTableName(){
        return 'user_roles';
    }

    static getIdField(){
        return 'id';
    }


}

module.exports = User;