/**
 * Created by priitpl.
 */
'use strict';

let BaseApi = require('./base');


class Group extends BaseApi {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_groups';
    }

    static getFields(){
        return {
            name: {nullable: false},
            is_default: {
                nullable: false,
                default: false,
                type: 'boolean'
            },
            monthly_fee: {
                nullable: false,
                default: 0,
                type: 'float'
            },
            maximum: {
                nullable: true,
                default: null,
                type: 'float'
            }
        }
    }
}

module.exports = Group;