/**
 * Created by priitpl.
 */
'use strict';

let BaseApi = require('./base');


class User extends BaseApi {
    constructor(data){
        super(data);
    }

    static getFields() {
        return {
            username: {
                validator: 'email',
                nullable: false
            },
            password: {
                nullable: false,
                type: 'password'
            },
            user_type:{
                model: 'user_types',
                nullable: false
            }

        };
    }

    static getTableName(){
        return 'user_users';
    }

    static getIdField(){
        return 'id';
    }


}

module.exports = User;