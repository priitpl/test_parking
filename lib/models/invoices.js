/**
 * Created by priitpl.
 */
'use strict';

let BaseApi = require('./base');


class Invoie extends BaseApi {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_groups';
    }

    static getFields(){
        return {
            client_id: {nullable: false},
            date_since: {
                nullable: false,
                type: 'date'
            },
            date_till: {
                nullable: false,
                type: 'date'
            },
            sum_total:{
                nullable: false,
                default: 0,
                type: 'decimal'
            }
        }
    }
}

module.exports = Invoie;