/**
 * Created by priitpl.
 */
'use strict';

let BaseModel = require('./base');


class ParkingManager extends BaseModel {
    constructor(data){
        super(data);
    }

    static getTableName(){
        return 'parking_managers';
    }

    static getFields(){
        return {
            user_id: {
                nullable: false,
                external: BaseModel.getModel('users')
            },
            company_id:{
                nullable: false,
                external: BaseModel.getModel('parking_companies')
            },
            valid_since: {
                nullable: true,
                type: 'date'
            },
            valid_till:{
                nullable: true,
                type: 'date'
            }
        }
    }
}

module.exports = ParkingManager;