/**
 * Created by priitpl.
 */
'use strict';

let BaseApi = require('./base');


class UserTypeApi extends BaseApi {
    constructor(){
        super();
    }

    static getFields(){
        return {
            name: {nullable: false}
        };
    }

    static getIdField(){
        return 'id';
    }

    static getTableName(){
        return 'parking_user_types';
    }
}

module.exports = UserTypeApi;