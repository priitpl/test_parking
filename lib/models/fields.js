/**
 * Created by priitpl.
 */


'use strict';



class Field{

    constructor(options){
        this.nullable = true;
        this.default = null;
    }


    isValid(value){
        return true;
    }


    static boolean(){

    }

    static integer(){

    }

    static decimal(){

    }

    static text(){

    }

    static string(){

    }
    static date(){

    }
    static time(){

    }
}

class Boolean extends Field{
    constructor(){
        super();
    }
}

class Integer extends Field{
    constructor(){
        super();
    }
}

class Decimal extends Field{
    constructor(){
        super();
    }
}
class Text extends Field{
    constructor(){
        super();
    }
}
class String extends Field{
    constructor(){
        super();
    }
}
class Date extends Field{
    constructor(){
        super();
    }
}
class Datetime extends Field{
    constructor(){
        super();
    }
}
class Email extends String{
    constructor(){
        super();
    }
}
