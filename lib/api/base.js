/**
 * Created by priitpl.
 */

'use strict';

const fs = require('fs');
const Promise = require('bluebird');
const nconf = require('../config');
const mysqlQuery = require('../mysql');

const SUFFIX = 'api';
const MODEL_REG = new RegExp(`\.${SUFFIX}\.js$`);

/**
 * Retrieve all models names
 */
const modelsNames = fs.readdirSync(__dirname)
        .filter(file => file.match(MODEL_REG))
        .map(file => file.replace(MODEL_REG, ''))
    ;
