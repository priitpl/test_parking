/**
 * Created by priitpl.
 */

'use strict';


const nconf = require('./config');
const express = require('express');
const bodyParser = require('body-parser')
const api = require('./models/base');
var path = require('path');

let app = express();

/*
 * Get and store HTTP_PORT
 */
const HTTP_PORT = nconf.get('http_port');


/*
 * Set static files to be processed by express
 * @todo - angular front-end part has to get this folders too from config. Right now hardcoded.
 */
app.use("/public", express.static(path.resolve(__dirname + '/../' + nconf.get('http_static'))));
app.use("/vendor", express.static(path.resolve(__dirname + '/../' +  nconf.get('bower_components'))));

/*
 * Tell express to use JSON in incoming requests.
 */
app.use(bodyParser.json());


/*
 * Bind all Models routs to express
 */
api.bind('/api/models/v1', app, express);


/*
 * Set root entry point for website.
 */
app.get("/", (req, res) => {
    res.sendFile(path.resolve(__dirname + '/../views/index.html'));
});

/*
 * Start listening on port defined in configs.
 */
app.listen(HTTP_PORT, () => {
    console.log(`Listening on port: ${HTTP_PORT}`);
});

module.exports = app;