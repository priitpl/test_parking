**Requirements**

Node JS > 4.x
npm - nodejs package manager
bower - package manager for web.
gulp - for running task.
MySQL - as main database.


**Installation:**

Install Node, NPM package manager, mysql, bower, gulp.

Copy of config.json.dist to config.json and change options in new file.

Then run given commands in your console:
-> npm install
-> bower install
-> gulp


**Testing:**
To test server side code, run from console in package root directory:
-> npm test

**Running server**
To run server, run from console:
-> npm start

If all dependencies were installed successfully and mysql server exists,
it will start server on port specified in config.json.