/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking')
        .directive('pagination', function(){
            return {
                controller: function(){

                },
                templateUrl: '/public/js/components/pagination/views/pagination.html'
            }
        })
    ;
})(angular);