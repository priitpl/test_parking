(function(angular){
    "use strict";
    angular.module('parking')
        .provider('ModelsConfig', function(){
            var apiRoot = '';
            this.setApiRoot = function(root){
                apiRoot = root.replace(/\/+$/, '');
            }
            this.$get = function(){
                return {
                    apiRoot: function(){
                        return apiRoot;
                    }
                }
            }
        })

        .config(['$resourceProvider', function ($resourceProvider) {
            $resourceProvider.defaults.actions.query = {
                method: 'GET',
                isArray: false
            };
        }])

        .factory('User', ['$resource', 'ModelsConfig', function($resource, ModelsConfig){
            return $resource(ModelsConfig.apiRoot() + '/users/:id', {id: '@id'});
        }])

        .factory('Contract', ['$resource', 'ModelsConfig', function($resource, ModelsConfig){
            return $resource(ModelsConfig.apiRoot() + '/contracts/:id', {id: '@id'});
        }])
        .factory('Company', ['$resource', 'ModelsConfig', function($resource, ModelsConfig){
            return $resource(ModelsConfig.apiRoot() + '/companies/:id', {id: '@id'});
        }])

        .factory('Group', ['$resource', 'ModelsConfig', function($resource, ModelsConfig){
            return $resource(ModelsConfig.apiRoot() + '/groups/:id', {id: '@id'});
        }])

        .factory('Place', ['$resource', 'ModelsConfig', function($resource, ModelsConfig){
            return $resource(ModelsConfig.apiRoot() + '/places/:id', {id: '@id'});
        }])

        .factory('Record', ['$resource', 'ModelsConfig', function($resource, ModelsConfig){
            return $resource(ModelsConfig.apiRoot() + '/records/:id', {id: '@id'});
        }])

        .factory('Company', ['$resource', 'ModelsConfig', function($resource, ModelsConfig){
            return $resource(ModelsConfig.apiRoot() + '/companies/:id', {id: '@id'});
        }])

        .factory('Invoice', ['$resource', 'ModelsConfig', function($resource, ModelsConfig){
            return $resource(ModelsConfig.apiRoot() + '/invoices/:id', {id: '@id'});
        }])

    ;
})(angular);