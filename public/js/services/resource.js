/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking')
        .provider('parkingResource', function(){
            var resource = null;

            this.setResource = function(_resource){
                console.log("SET RESOURCE", _resource.$get);
                resource = _resource;
            };

            this.$get = function(){
                return {
                    getResource: function(){
                        return resource
                    }
                }
            }
        })
    ;
})(angular);