/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking')
        .directive('pModel', function(){
            return {
                restrict: 'EA',
                controller: function(){
                    console.log("parking_form:controller", arguments);
                },
                link: function(scope, element, attrs, controllers){
                    console.log("parking_form:link", arguments);
                }
            }
        })
    ;
})(angular);