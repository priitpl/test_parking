/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking')
        .directive('parkingConfirmClick', function(){
            return {
                restrict: 'AE',
                link: function (scope, element, attr) {
                    console.log("parkingConfirmClick");
                    var message = attr.parkingConfirmClick || "Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click',function (event) {
                        if ( window.confirm(message) ) {
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
        })
    ;
})(angular);

