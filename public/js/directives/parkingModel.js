/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking')
        .directive('parkingModel', ['Company', function(Company){
            return {
                restrict: 'EA',
                controller: ['$scope', function($scope){
                    $scope.getModel = function(){
                        return Company;
                    }
                    this.a = "AAA";
                    $scope.b ='BBB';
                }],
                link: function(scope, element, attr, controllers){
                    console.log("Add getModel into scope", controllers);
                    scope.$parent.getModel = function(){
                        return Company;
                    }
                }
            }
        }])
    ;
})(angular);