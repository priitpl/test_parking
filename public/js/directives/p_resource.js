/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking')
        .directive('pResource', function(){
            return {
                restrict: 'EA',
                controller: function($scope){
                    console.log("parking_resource:controller", arguments);
                    $scope.thisIsPResource = true;

                },
                link: function(scope, element, attrs, controllers){
                    console.log("parking_resource:link", arguments);
                }
            }
        })
    ;
})(angular);