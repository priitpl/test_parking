/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking')
        .directive('pResourceIds', function(){
            return {
                restrict: 'EA',
                controller: function($scope){
                    console.log("parking_resource_ids:controller", arguments);
                    $scope.thisIsPResourceIds = true;
                },
                link: function(scope, element, attrs, controllers){
                    console.log("parking_resource_ids:link", arguments);
                }
            }
        })
    ;
})(angular);