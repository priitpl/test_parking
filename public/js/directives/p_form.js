/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking')
        .directive('pForm', function(){
            return {
                restrict: 'EA',
                controller: ['$scope', '$q', function($scope, $q){
                    $scope.thisIsPForm = true;


                    /**
                     * Main function for creating/updateing entity.
                     * Forwards to createNew() update() methods.
                     * @returns {*}
                     */
                    function save(){
                        if (entity.getId()) {
                            return update();
                        } else {
                            return createNew();
                        }

                    }

                    /**
                     * Delete entity.
                     */
                    function deleteRecord(){
                        var def = $q.defer();


                        return def.promise;
                    }

                    /**
                     * Updates record.
                     */
                    function update(){
                        var def = $q.defer();

                        return def.promise;
                    }


                    /**
                     * Creates new records.
                     */
                    function createNew(){
                        var def = $q.defer();

                        def.resolve(function(){
                            var data = new Array(100);
                            data.fill(null).map(function(el, i){
                                return {
                                    id: i,
                                    data: null
                                };
                            });
                        });
                        return def.promise;
                    }


                    /**
                     * Retrieve all elements.
                     */
                    function getList(){
                        var def = $q.defer();
                        def.resolve(new Array(100).fill(null).map());
                        return def.promise;
                    }
                }],
                require: ['pResource', 'pResourceIds'],
                link: function(scope, element, attrs, controllers){
                    console.log("parking_form:link", arguments);
                }
            }
        })
    ;
})(angular);