/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.admin', [])
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('admin', {
                    url: "/admin",
                    controller: ['$scope', function($scope){
                        $scope.data = new Array(10);

                        $scope.data = $scope.data.fill(null).map(function(el, i){
                            return {
                                id: i,
                                name: 'Vasja',
                                email: 'email@address.ee',
                                password: 'hren:' + i,
                                type: 'customer'
                            };
                        });

                        $scope.login = function(){
                            console.log("users login", arguments);
                        };
                        $scope.delete = function(){
                            console.log("users delete", arguments);
                        }
                    }],
                    templateUrl: "/public/js/sections/admin/views/index.html"
                })
            ;
        }])
})(angular);