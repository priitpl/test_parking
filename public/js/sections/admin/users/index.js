/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.admin')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('admin.users', {
                    url: "/users",
                    templateUrl: "/public/js/sections/admin/users/views/index.html"
                })
                .state('admin.users.edit', {
                    url: "/edit/{id}",
                    templateUrl: "/public/js/sections/admin/users/views/form.html"
                })
                .state('admin.users.create', {
                    url: "/create",
                    templateUrl: "/public/js/sections/admin/users/views/form.html"
                })
            ;
        }])
})(angular);