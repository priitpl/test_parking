/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.admin')
        .controller('UsersController', ['$scope', 'User', function($scope, User){
            $scope.getModel = function(){
                return User;
            }
        }])
})(angular);