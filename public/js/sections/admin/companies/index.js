/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.admin')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('admin.companies', {
                    url: "/companies",
                    templateUrl: "/public/js/sections/admin/companies/views/index.html"
                })
                .state('admin.companies.edit', {
                    url: "/edit/{id}",
                    templateUrl: "/public/js/sections/admin/companies/views/form.html"
                })
                .state('admin.companies.create', {
                    url: "/create",
                    templateUrl: "/public/js/sections/admin/companies/views/form.html"
                })
            ;
        }])
})(angular);