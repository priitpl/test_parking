/**
 * Created by priitpl.
 */

/**
 * Created by priitpl.
 */


(function(angular){
    "use strict";
    angular.module('parking.sections', [])
        .provider('$SectionsTemplate', [function(){
            var sectionsRootPath = '/public/js/sections/';

            function path(section, template){
                section = section.split('.').join('/');
                return sectionsRootPath + section + '/views/' + template + '.html'
            }


            /**
             * Set sections root path.
             * @param path
             */
            this.setSectionsRootPath = function(path){
                sectionsRootPath = path;
            }

            /**
             * Provider API.
             */
            var api = {
                path: path
            };

            /**
             * Path
             * @returns {{path: path}}
             */
            this.$get = function(){
                return api
            };
        }])
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('index', {
                    url: "",
                    templateUrl: "/public/js/sections/views/index.html"
                })
            ;
        }])
    ;
})(angular);