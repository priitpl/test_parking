/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking.sections.my', [])
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('my', {
                    url: "/my",
                    templateUrl: "/public/js/sections/my/views/index.html"
                })
            ;
        }])
    ;
})(angular);