/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.my')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('my.parking', {
                    url: '/parking',
                    controller: ['$scope', '$Helpers', function($scope, $Helpers){
                        $scope.data = new Array(20);
                        $scope.data = $scope.data.fill(null).map(function(el, i){
                            return {
                                id: i,
                                car_number: '294 BBG',
                                place_name: 'Laki 25',
                                company_name: $Helpers.randomBoolean() ? 'Uhisteenused' : 'City parkla',
                                started_at: new Date(),
                                finished_at: $Helpers.randomBoolean() ? new Date() : null,
                                hours: Math.ceil(Math.random() * 10),
                                minutes: Math.ceil(Math.random() * 10),
                                price: Math.ceil(Math.random() * 10)
                            }
                        })
                    }],
                    templateUrl: "/public/js/sections/my/parking/views/index.html"
                })
            ;
        }])
    ;
})(angular);
