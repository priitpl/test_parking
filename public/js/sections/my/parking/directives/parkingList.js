/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.my')
        .directive('parkingTable', function(){
            return {
                restrict: 'EA',
                controller: 'ParkingListController',
                templateUrl: '/public/js/sections/my/parking/views/parking_list.html'
            };
        })
    ;
})(angular);