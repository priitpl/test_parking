/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.my')
        .controller('ParkingListController', ['$scope', '$Api', function($scope, $Api){
            $scope.data = [];
            $Api.getParkingRecords().then(function(result){
                $scope.data = result;
            });

        }])
    ;
})(angular);