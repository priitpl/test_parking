/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.my')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('my.invoices', {
                    url: '/invoices',
                    controller: ['$scope', '$Helpers', function($scope, $Helpers){
                        $scope.data = new Array(10);
                        $scope.data = $scope.data.fill(null).map(function(el,i ){
                            var a = {
                                id: i,
                                invoice_number: 'ABC-' + Math.ceil(Math.random() * 1000),
                                company_name: $Helpers.randomBoolean() ? 'Uhisteenused' : 'City parkla',
                                period_start: new Date(),
                                period_end: new Date(),
                                parking_total: Math.ceil(Math.random() * 20),
                                sum_total: Math.ceil(Math.random() * 150),
                                is_payed: $Helpers.randomBoolean(),
                                pdf_link: null
                            }
                            a.payed_on = a.is_payed ? new Date() : null;
                            return a;
                        });
                    }],
                    templateUrl: "/public/js/sections/my/invoices/views/index.html"
                })
                .state('my.invoices.invoice', {
                    url: '/{id}',
                    templateUrl: "/public/js/sections/my/invoices/views/invoice.html"
                })
                .state('my.invoices.pdf', {
                    url: '/pdf/{id}',
                    templateUrl: "/public/views/parking.html"
                })
            ;
        }])
    ;
})(angular);
