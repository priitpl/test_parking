/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.manager', [])
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('manager', {
                    url: "/manager",
                    templateUrl: '/public/js/sections/manager/views/index.html'
                    // templateUrl: $SectionsTemplateProvider.path('manager', 'index')
                })
            ;
        }])
    ;
})(angular);