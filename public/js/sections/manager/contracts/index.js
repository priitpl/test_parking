/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.manager')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('manager.contracts', {
                    url: "/contracts",
                    controller: ['$scope', '$Helpers', function($scope, $Helpers){
                        $scope.data = new Array(10);
                        $scope.data = $scope.data.fill(null).map(function(el, i){
                            return {
                                id: i,
                                client_name: 'Vasja',
                                group: 'VIP' + $Helpers.randomBoolean() ? ' 1': null,
                                client_id: i + 10,
                                car_numbers: ['294 BBG'],
                                valid_since: new Date(),
                                valid_till: $Helpers.randomBoolean() ? new Date() : null
                            };
                        });
                    }],
                    templateUrl: '/public/js/sections/manager/contracts/views/index.html'
                })
                .state('manager.contracts.new', {
                    url: "/new",
                    templateUrl: '/public/js/sections/manager/contracts/views/form.html'
                })
                .state('manager.contracts.edit', {
                    url: "/{id}",
                    templateUrl: '/public/js/sections/manager/contracts/views/form.html'
                })
            ;
        }])
    ;
})(angular);