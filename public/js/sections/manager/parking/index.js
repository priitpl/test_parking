/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.manager')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('manager.parking', {
                    url: "/parking",
                    controller: ['$scope', function($scope){
                        $scope.data = new Array(10);
                        $scope.data = $scope.data.fill(null).map(function(el, i){
                            return {
                                id: i+1,
                                name: 'Laki 25',
                                address: 'Laki 25',
                                car_number: '294 BBG',
                                since: new Date(),
                                till: !!(Math.ceil(Math.random() * 10) % 2) ? new Date() : null
                            };
                        });
                    }],
                    templateUrl: '/public/js/sections/manager/parking/views/index.html'
                })
            ;
        }])
    ;
})(angular);