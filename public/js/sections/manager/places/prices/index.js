/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.manager')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('manager.places.prices', {
                    url: "/{id}/prices",
                    controller: ['$scope', function($scope){
                        $scope.data = new Array(4);
                        $scope.data = $scope.data.fill(null).map(function(el, i){
                            return {
                                id: i+1,
                                name: 'Laki 2' + i,
                                address: 'Laki 2' + i
                            };
                        });

                        $scope.groups = new Array(3);
                        $scope.groups = $scope.groups.fill(null).map(function(el, i){
                            return {
                                id: i,
                                name: 'VIP ' + i,
                                is_default: false
                            }
                        })
                        $scope.groups[0].name = "Default";
                        $scope.groups[0].is_default = true;

                    }],
                    templateUrl: '/public/js/sections/manager/places/prices/views/index.html'
                })
            ;
        }])
    ;
})(angular);