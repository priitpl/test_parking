/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.manager')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('manager.places', {
                    url: "/places",
                    controller: ['$scope', 'Place', function($scope, Place){
                        // $scope.data = new Array(10);
                        // $scope.data = $scope.data.fill(null).map(function(el, i){
                        //     return {
                        //         id: i+1,
                        //         name: 'Laki 2' + i,
                        //         car_places: Math.ceil(Math.random() * 20),
                        //         address: 'Laki 2' + i
                        //     };
                        // });

                        $scope.data = Place.query();

                    }],
                    templateUrl: '/public/js/sections/manager/places/views/index.html'
                })
                .state('manager.places.new', {
                    url: '/new',
                    templateUrl: '/public/js/sections/manager/places/views/form.html'
                })
                .state('manager.places.edit', {
                    url: '/{id}',
                    templateUrl: '/public/js/sections/manager/places/views/form.html'
                })
            ;
        }])
    ;
})(angular);