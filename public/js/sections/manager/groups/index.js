/**
 * Created by priitpl.
 */

(function(angular){
    "use strict";
    angular.module('parking.sections.manager')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('manager.groups', {
                    url: "/groups",
                    controller: ['$scope', '$Helpers', function($scope, $Helpers){
                        $scope.data = new Array(10);
                        $scope.data = $scope.data.fill(null).map(function(el, i){
                            return {
                                id: i,
                                name: "Vip ",
                                is_default: false,
                                customers_total: Math.ceil(Math.random() * 10),
                                valid_since: new Date(),
                                valid_till: $Helpers.randomBoolean() ? new Date() : null
                            }
                        });

                        $scope.data[0].name = 'Default';
                        $scope.data[0].is_default = true;
                    }],
                    templateUrl: '/public/js/sections/manager/groups/views/index.html'
                })
                .state('manager.groups.new', {
                    url: "/new",
                    templateUrl: '/public/js/sections/manager/groups/views/form.html'
                })
                .state('manager.groups.edit', {
                    url: "/{id}",
                    templateUrl: '/public/js/sections/manager/groups/views/form.html'
                })
            ;
        }])
    ;
})(angular);