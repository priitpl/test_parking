/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking')
        .controller('TableViewController', ['$scope', '$state', function($scope, $state){
            console.log("TableViewController", $scope);
            $scope.items = [];
            $scope.getModel().query(function(resource){
                if (resource.success) {
                    $scope.items = resource.items;
                } else {
                    $scope.error = resource.error;
                }
            });
        }])
    ;
})(angular);