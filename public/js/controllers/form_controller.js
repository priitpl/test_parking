/**
 * Created by priitpl.
 */
(function(angular){
    "use strict";
    angular.module('parking')
        .controller('FormController', ['$scope', '$state', function($scope, $state){

            console.log("FormController", $state.params.id);

            if ($state.params.id){
                $scope.getModel().get({id: $state.params.id}, function(response){
                    // If went wrong.
                    if (!response.success || !response.item) {

                    } else {
                        $scope.item = response.item;
                    }
                });
            }

            $scope.createAction = function(){
                console.log("Create ACtion", $scope.getModel());
                var model = $scope.getModel();
                var item = new model($scope.item);
                item.$save();
                console.log("Item", item);
            }

            $scope.updateAction = function(){
                console.log("Update ACtion", $scope.getModel());
            }

            $scope.deleteAction = function(){
                console.log("Delete ACtion", $scope.getModel());
            }
        }])
    ;
})(angular);