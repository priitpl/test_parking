/**
* Created by priitpl.
*/
(function(angular){
    "use strict";
    angular.module('parking', ['ngResource', 'ui.router', 'parking.sections', 'parking.sections.admin', 'parking.sections.my', 'parking.sections.manager', 'parking.prices'])
        // Here we define which resource service to use for generating models.
        .config(['parkingResourceProvider', '$resourceProvider', function(parkingResourceProvider, $resourceProvider){
            parkingResourceProvider.setResource($resourceProvider);
            console.log("Parking resource provider");
        }])

        // Set Models API root path
        .config(['ModelsConfigProvider', function(ModelsConfigProvider){
            ModelsConfigProvider.setApiRoot('/api/models/v1');
        }])

        // Default controller
        .controller('MainController', ['$scope', function($scope){
            console.log("init");
        }])
        .service('$Helpers', [function(){
            var helpers = {
                randomBoolean: function(){
                    return !(Math.ceil(Math.random() * 10) % 2)
                },
                randomDate: function(){
                    var date = new Date();
                    return date;
                }
            };
            return helpers;
        }])
    ;
})(angular);
