/**
 * @author priit
 */

"use strict";

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    less   = require('gulp-less'),
    fs = require('fs')
;

const jsFolder = __dirname + '/public/js/';


// Concatenate JS Files
gulp.task('build', function() {
    return gulp.src(
            [`${jsFolder}app.js`]
            .concat([`${jsFolder}services/*.js`, `${jsFolder}directives/*.js`], `${jsFolder}controllers/*.js`)
            .concat(angluarSections())
            .concat(angluarSections(`${jsFolder}components/`))
            // .concat(componentFiles('prices'))
            // .concat(sectionFiles('parking'))
        )
        .pipe(concat('main.js'))
        .pipe(gulp.dest('public/build/js'));
});

gulp.task('jquery', bowerLibTask(['bower_components/jquery/dist/jquery.js'], 'jquery.js'));
gulp.task('angular', bowerLibTask(['bower_components/angular/angular.js'], 'angular.js'));
gulp.task('resource', bowerLibTask(['bower_components/angular-resource/angular-resource.js'], 'resource.js'));
gulp.task('ui-router', bowerLibTask(['bower_components/ui-router/release/angular-ui-router.js'], 'ui-router.js'));

gulp.task('less', function () {
    return gulp.src('./less/*.less')
        .pipe(less())
        .pipe(gulp.dest('./public/css'));
});

// Default Task
gulp.task('default', ['build', 'jquery', 'angular', 'resource', 'ui-router', 'less']);

/**
 * Get Angular component files regx
 * @param component
 * @returns {*[]}
 */
function componentFiles(component){
    return libFiles('components', component);
}

function sectionFiles(section){
    return libFiles('sections', section);
}

function libFiles(start, end){
    return [
        jsFolder + start + "/" + end + '/index.js',
        jsFolder + start + "/" + end + '/controllers/*.js',
        jsFolder + start + "/" + end + '/controllers/**/*.js',
        jsFolder + start + "/" + end + '/services/*.js',
        jsFolder + start + "/" + end + '/services/**/*.js',
        jsFolder + start + "/" + end + '/directives/*.js',
        jsFolder + start + "/" + end + '/directives/**/*.js'
    ];
}

function bowerLibTask(src, to){
    return function() {
        gulp.src(src)
            .pipe(concat(to))
            .pipe(gulp.dest('public/build/js'));
    }
}

function angluarSections(dir){
    let result = [];

    dir = dir || jsFolder + 'sections/';

    let dirStructure = fs.readdirSync(dir);

    let subFolders = dirStructure.filter(file => {
        return fs.statSync(dir + file).isDirectory();
    });

    let subFiles = dirStructure.filter(file => {
        return !fs.statSync(dir + file).isDirectory()
            && file.match(/\.js$/)
            ;
    });

    result = subFiles
        .filter(file => file == 'index.js')
        .map(file => dir + file)
        .concat(subFiles
            .filter(file => file != 'index.js')
            .map(file => dir + file)
        );

    subFolders.forEach((folder) => {
        result = result.concat(angluarSections(dir + folder + "/"));
    });

    return result;
}
